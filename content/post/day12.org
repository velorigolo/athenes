#+TITLE: Jour 12
#+DATE: 2022-09-01T10:17:43Z
#+AUTHOR: Quentin
#+DRAFT: false
#+COVER: day12.jpg
#+PLACE: Brindisi
#+SUMMARY: Hiérarchie des cyclistes
#+MAP: true

** Etape 

Bari - Brindisi

** Kilomètres

125

** Météo

Petite bruine, pas de soleil, ça fait du bien

** Phrase du jour

'Nos jambes sont bien remplies de glycogène après deux jours de repos' -
Mathieu

** Résumé

Départ ce matin un peu spécial après nos 2 jours de repos à Bari, car Mathieu
n'a pas récupéré son vélo chez le réparateur italien qui ferme à 12h30 en
semaine. Nous partons donc moi en vélo, lui en trotinette, pour récupérer son
vélo. À l'arrivée, j'entends Mathieu et le réparateur rire beaucoup dans le
magasin pendant que je me tartine de crème solaire. Mathieu sort quelques
minutes plus tard en me disant qu'il a payé 30 euros pour absolument rien vu
que le réparateur n'a pas eu le temps de changer ce qui était cassé. Je
comprends mieux le rire du point de vue de l'italien, mais moins du côté de
Mathieu.

L'étape est très roulante. Nos corps ont enfin passé le seuil difficile des 10
premiers jours ou les muscles sont fatigués et la récupération difficile. Ils
sont adaptés à l'effort. Nous déroulons à quasiment 24 km/h de moyenne durant
toute l'étape. De plus, le temps est à la pluie fine, et nous ne surchauffons
pas. A part la fin de l'étape ou la pluie devient bien énervée et nous forcera
à nettoyer nos vélos plein de boue avant de rentrer dans l'hôtel, je n'ai pas
grand chose de plus à dire sur cette dernière étape italienne et je vais enfin
pouvoir vous parler de la

HIÉRARCHIE DES CYCLISTES À VÉLO SUR LES ROUTES ITALIENNES (que je prépare dans
ma tête depuis le premier jour du voyage) En effet rouler sur les routes
italiennes est extrêmement intéressant du point de vue des autres cyclistes
que nous croisons. L'Italie a une grande culture du vélo et on y croise les
mêmes vieux courbés sur leur vélo qu'en Asie Centrale et les mêmes bourrins du
vélo de route qu'en France. J'ai donc décidé de classer les typologies de
cyclistes par niveau de puissance pour plus de clarté :

Niveau 0 : Retraité sur guimbarde de 1945

Ces cyclistes sont le niveau 0 de la hiérarchie pour la simple raison qu'ils
se déplacent à la même vitesse qu'un piéton (environ 4 km/h, 5 quand le vent
est dans le dos). Ils n'ont pas compris qu'il fallait monter la selle un
minimum pour bien pédaler, et possèdent souvent un grand panier avec des
légumes de leur champ qu'ils sèment sur la route pour ne pas perdre leur
chemin

Difficulté à gérer : 5/10 attention aux légumes qui tombent

Niveau 1 : L'adolescent sur son fatbike

Ce spécimen se croise majoritairement près des stations balnéaires. Le
fatbike, supposé servir pour rouler sur du sable, n'est généralement pas du
tout exploité pour cette raison, mais plutôt pour frimer auprès des autres
adolescents nés après 2010. Il est important de baisser la selle au maximum
pour faire comme sur une moto et de rouler le plus lentement possible de
préférence avec une enceinte crachant du rap italien à plein pot.

Difficulté à gérer : 2/10 il faut juste supporter le désagrément quand on les
dépasse.

Niveau 2 : La famille avec enfants

Pour être comptabilisée, il faut au moins 2 enfants dont un de moins de 6 ans
qui roule en faisant de grandes embardées vers la gauche et la droite et un
parent fatigué qui ne surveille plus du tout les gamins. Ils se déplacent
entre 8 et 12km/h, certains bambins étant dans un chariot derrière le vélo de
papa, permettant d'aller plus vite

Difficulté à gérer : 9/10 les enfants sont très accidentogènes.

Niveau 3 : Le cycliste lambda

Il est habillé normalement, ressemble à toute le monde, il pédale normalement
(15-18 km/h), pour aller ou rentrer du travail, il regarde Netflix le soir
quand il rentre chez lui si il a moins de 35 ans, la télé italienne si il a
plus de 35 ans, travaille sûrement dans une boite de com ou à la poste, et il
rêve de partir en vacances en Floride. Parfois il a un vélo électrique et il
va un peu plus vite, pour pimenter sa vie.

Difficulté à gérer : 5/10, la moyenne.

Niveau 4 : Le cyclorandonneur

Il est parti avec 2 slips et 2 t-shirts il y'a 3 ans, ne lave jamais ses
chaussettes, à appris à coudre des motifs traditionnels nigérians lors d'un
précédent voyage ou il a rencontré des personnes incroyables 'si pauvres mais
si riches en même temps !'. Il était trader avant mais il en a eu marre et
c'est quand même plus facile de partir autour du monde quand on a gagné 3
millions de dollars usd en 2 stages chez Goldman Sachs. Ça ne l'empêche pas
d'essayer de se faire loger chez l'habitant sans rien payer. Il a des mollers
d'acier donc il se déplace vite (25km/h de moyenne).

Difficulté à gérer : 10/10 car il vous dit bonjour, essaie de savoir d'ou vous
venez, vous explique que lui il est plus fort, plus rapide et va plus loin que
vous (et plus sale mais il s'en vante moins) et propose de faire un bout de
route ensemble.

Niveau 5 : le cycliste de route

Il possède un vélo à 5000 euros, peut-être un gravel selon la saison. Sa
transmission est électrique car un système mécanique pèse 45 grammes de
plus. Il est connecté à 12 applications en même temps, une pour le guidage,
une pour le VO2max, une pour les calories et apports nutritifs et une pour
ouvrir la porte de son garage et ainsi de suite. En dessous de 35 km/h il se
mets en jambes. Il est parti à 5h du matin et il en est a son troisième col
hors catégorie et il arrive tranquillement sur son 167ie kilomètre
aujourd'hui. Il a déjà parlé à Geraint Thomas et pense que le dopage n'existe
pas et que les gens qui pensent le contraire sont jaloux.

Difficulté à gérer : 0/10, il ne t'accordera aucun regard et te dépassera à
toute vitesse. Le temps que tu clignes des yeux il a déjà disparu.

Niveau 6 : Le vieux torse nu sur son VTT

Il fait du vélo dans ces campagnes quotidiennement depuis 60 ans. Il ne le dit
pas mais dans les années 70 il était le compagnon de sortie d'Eddy Merckx. Il
est tellement bronzé que sa peau repousse les rayons du soleil le faisant
briller de mille feux. Fume 2 paquets de gitane sans filtre et boit une
bouteille de Limoncello par jour. Son vélo est un modèle VTT sans marque d'une
valeur de 7.5 euros à la reprise. On regarde de près pour voir si son vélo est
doté d'un moteur 200 CC mais non, c'est juste ses mollets en acier trempé dans
l'huile d'olive. Se déplace plus vite qu'une voiture, au moins. Semble
toujours se diriger dans la même direction que vous pour que vous vous sentiez
humiliés.

Difficulté à gérer : 13/10 pour l'estime de soi décapitée.


